exec: exec.o gfxlib/libisentlib.a define.o hmi.o calculus.o
	gcc -Wall exec.o -o exec gfxlib/libisentlib.a define.o hmi.o calculus.o -lm -lglut -lGL -lX11

gfxlib/libisentlib.a: gfxlib/BmpLib.o gfxlib/ErreurLib.o gfxlib/ESLib.o gfxlib/GfxLib.o gfxlib/OutilsLib.o gfxlib/SocketLib.o gfxlib/ThreadLib.o gfxlib/TortueLib.o gfxlib/VectorLib.o gfxlib/WavLib.o
	ar r gfxlib/libisentlib.a gfxlib/BmpLib.o gfxlib/ErreurLib.o gfxlib/ESLib.o gfxlib/GfxLib.o gfxlib/OutilsLib.o gfxlib/SocketLib.o gfxlib/ThreadLib.o gfxlib/TortueLib.o gfxlib/VectorLib.o gfxlib/WavLib.o
	ranlib gfxlib/libisentlib.a

exec.o: exec.c gfxlib/GfxLib.h gfxlib/BmpLib.h gfxlib/ESLib.h define.h hmi.h
	gcc -Wall -c -std=c99 exec.c

define.o: define.c gfxlib/GfxLib.h gfxlib/BmpLib.h gfxlib/ESLib.h define.h
	gcc -Wall -c -std=c99 define.c

hmi.o: hmi.c gfxlib/GfxLib.h gfxlib/BmpLib.h gfxlib/ESLib.h define.h hmi.h
	gcc -Wall -c -std=c99 hmi.c

calculus.o: calculus.c gfxlib/GfxLib.h gfxlib/BmpLib.h gfxlib/ESLib.h define.h calculus.h
	gcc -Wall -c -std=c99 calculus.c

gfxlib/BmpLib.o: gfxlib/BmpLib.c gfxlib/BmpLib.h gfxlib/OutilsLib.h
	gcc -Wall -O2 -c gfxlib/BmpLib.c

gfxlib/ESLib.o: gfxlib/ESLib.c gfxlib/ESLib.h gfxlib/ErreurLib.h
	gcc -Wall -O2 -c gfxlib/ESLib.c

gfxlib/ErreurLib.o: gfxlib/ErreurLib.c gfxlib/ErreurLib.h
	gcc -Wall -O2 -c gfxlib/ErreurLib.c

gfxlib/GfxLib.o: gfxlib/GfxLib.c gfxlib/GfxLib.h gfxlib/ESLib.h
	gcc -Wall -O2 -c gfxlib/GfxLib.c -I/usr/include/GL

gfxlib/OutilsLib.o: gfxlib/OutilsLib.c gfxlib/OutilsLib.h
	gcc -Wall -O2 -c gfxlib/OutilsLib.c

gfxlib/SocketLib.o: gfxlib/SocketLib.c gfxlib/SocketLib.h
	gcc -Wall -O2 -c gfxlib/SocketLib.c

gfxlib/ThreadLib.o: gfxlib/ThreadLib.c gfxlib/ThreadLib.h
	gcc -Wall -O2 -c gfxlib/ThreadLib.c

gfxlib/TortueLib.o: gfxlib/TortueLib.c gfxlib/TortueLib.h gfxlib/GfxLib.h
	gcc -Wall -O2 -c gfxlib/TortueLib.c

gfxlib/VectorLib.o: gfxlib/VectorLib.c gfxlib/VectorLib.h
	gcc -Wall -O2 -c gfxlib/VectorLib.c -msse3

gfxlib/WavLib.o: gfxlib/WavLib.c gfxlib/WavLib.h gfxlib/OutilsLib.h
	gcc -Wall -O2 -c gfxlib/WavLib.c -Wno-unused-result

zip:
	tar -cvzf libisentlib.tgz *.[ch] *.bmp *.pdf makefile

clean:
	rm -f *~ *.o

deepclean: clean
	rm -f exemple exempleTortue libisentlib.a

