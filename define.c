/*
File    : define.h
Author : Arthur FRANCISCO
Created : 18/12/2017
Comment : File that contains all fonctions used in the project.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

#include "gfxlib/GfxLib.h"
#include "gfxlib/BmpLib.h"
#include "gfxlib/ESLib.h"

#include "define.h"

void cercle(float centreX, float centreY, float rayon)
{
	const int Pas = 20; // Nombre de secteurs pour tracer le cercle
	const double PasAngulaire = 2.*M_PI/Pas;
	int index;
	
	for (index = 0; index < Pas; ++index) // Pour chaque secteur
	{
		const double angle = 2.*M_PI*index/Pas; // on calcule l'angle de depart du secteur
		triangle(centreX, centreY,
				 centreX+rayon*cos(angle), centreY+rayon*sin(angle),
				 centreX+rayon*cos(angle+PasAngulaire), centreY+rayon*sin(angle+PasAngulaire));
			// On trace le secteur a l'aide d'un triangle => approximation d'un cercle
	}
	
}

void initState(int state[NB_STATE]){
    for(int i = 0; i < NB_STATE; i++){
        state[i] = 0;
    }
}

void initBoutons(bouton buttons[NB_BUTTONS]){
    for(int i = 0; i < NB_BUTTONS; i++){
        buttons[i].etat = 0;
    }

    //Bouton 0 (Nouvelle partie)
    strcpy(buttons[0].nom, "new_game");
    strcpy(buttons[0].pos.nom, "position");
    buttons[0].pos.x = LargeurFenetre/4;
    buttons[0].pos.y = 7*HauteurFenetre/12;
    buttons[0].bpSprite = lisBMPRGB("images/buttons/bp0.bmp");

    //Bouton 1 (règles)
    strcpy(buttons[1].nom, "rules");
    strcpy(buttons[1].pos.nom, "position");
    buttons[1].pos.x = LargeurFenetre/4;
    buttons[1].pos.y = 4*HauteurFenetre/12;
    buttons[1].bpSprite = lisBMPRGB("images/buttons/bp1.bmp");

    //Bouton 2 (exit)
    strcpy(buttons[2].nom, "exit");
    strcpy(buttons[2].pos.nom, "position");
    buttons[2].pos.x = LargeurFenetre/4;
    buttons[2].pos.y = HauteurFenetre/12;
    buttons[2].bpSprite = lisBMPRGB("images/buttons/bp2.bmp");

    //Bouton 3 (resume game)
    strcpy(buttons[3].nom, "resume");
    strcpy(buttons[3].pos.nom, "position");
    buttons[3].pos.x = 305;
    buttons[3].pos.y = 105+7*(495-105)/12;
    buttons[3].bpSprite = lisBMPRGB("images/buttons/bp3.bmp");

    //Bouton 4 (rules)
    strcpy(buttons[4].nom, "rules");
    strcpy(buttons[4].pos.nom, "position");
    buttons[4].pos.x = 305;
    buttons[4].pos.y = 105+4*(495-105)/12;
    buttons[4].bpSprite = lisBMPRGB("images/buttons/bp4.bmp");

    //Bouton 3 (back to menu)
    strcpy(buttons[5].nom, "back_to_menu");
    strcpy(buttons[5].pos.nom, "position");
    buttons[5].pos.x = 305;
    buttons[5].pos.y = 105+(495-105)/12;
    buttons[5].bpSprite = lisBMPRGB("images/buttons/bp5.bmp");
}

/* ****************************************************************** */
static int *BVR2ARVB_sansFond(int largeur, int hauteur, const unsigned char *donnees, couleur fond){
    const unsigned char *ptrDonnees;
    unsigned char *pixels = (unsigned char*)malloc(largeur*hauteur*sizeof(int));
    unsigned char *ptrPixel;
    int index;

    ptrPixel = pixels;
    ptrDonnees = donnees;
    for(index = largeur*hauteur; index != 0; --index){
        ptrPixel[0] = ptrDonnees[0];
        ptrPixel[1] = ptrDonnees[1];
        ptrPixel[2] = ptrDonnees[2];
        if(ptrDonnees[0] == fond.B && ptrDonnees[1] == fond.G && ptrDonnees[2] == fond.R)
            ptrPixel[3] = 0x000;
        else
            ptrPixel[3] = 0x0FF;
        ptrDonnees += 3;
        ptrPixel += 4;
    }
    return (int *)pixels;
}

void ecrisImageSansFond(int x, int y, int largeur, int hauteur, const unsigned char *donnees, couleur fond){
    int *pixels = BVR2ARVB_sansFond(largeur, hauteur, donnees, fond);
    ecrisImageARVB(x, y, largeur, hauteur, pixels);
    free(pixels);
}