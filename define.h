/*
File    : define.h
Authors : Yanis ATTIA | Arthur FRANCISCO
Created : 18/12/2017
Comment : File that contains all the definitions used in the project.
*/


/*
--------------- ESPACE DE DEFINITION DES CONSTANTES --------------------
*/

#define LargeurFenetre 800
#define HauteurFenetre 600

#define INCLINE 6.5
#define GRAVITY 9.81
#define BALL_RAY 10

#define M_PI 3.141592654
#define EPSILON 1

#define NB_STATE 10
#define NB_IMG 6
#define NB_BUTTONS 6
#define NB_RESSORT 5

#define NB_OBJETS 23

#define LCH 255

/*
--------------- ESPACE DE DEFINITION DES STRUCTURES --------------------
*/

typedef struct couleur{
    char nom[LCH];
    int R, G, B;
}couleur;

typedef struct position{
    char nom[LCH];
    int x, y;
}position;

typedef struct bouton{
    char nom[LCH];
    int etat;
    position pos;
    DonneesImageRGB *bpSprite;
}bouton;

typedef struct objet{
    char nom[LCH];
    position pos_init;
    position pos_end;
    position pos_initR;
    position pos_endL;
    float vx;
    float vy;
    float ax;
    float ay;
    int rayon;
    int forme;
    int vmodif;
    couleur color;
    //Contient chaque image du sprite (à définir avec la prof pour le **)
    //DonneesImageRGB **sprite;
}objet;

void cercle(float centreX, float centreY, float rayon);

void initState(int *state);

void initBoutons(bouton buttons[NB_BUTTONS]);

void ecrisImageSansFond(int x, int y, int largeur, int hauteur, const unsigned char *donnees, couleur fond);
