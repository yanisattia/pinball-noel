/*
File    : calculus.h
Author : Arthur FRANCISCO
Created : 18/12/2017
Comment : File that contains all the definitions used in the project.
*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

#include "gfxlib/GfxLib.h"
#include "gfxlib/BmpLib.h"
#include "gfxlib/ESLib.h"

#include "define.h"
#include "calculus.h"

objet initialiseObjet(char nom[LCH], int xInit, int yInit, int xEnd, int yEnd, int xInitR, int yInitR, int xEndL, int yEndL, int rayon, int forme, int vmodif, float vx, float vy, float ay){
    objet obj;
    strcpy(obj.nom, nom);
    position pos;
    pos.x = xInit;
    pos.y = yInit;
    obj.pos_init = pos;
    pos.x = xEnd;
    pos.y = yEnd;
    obj.pos_end = pos;
    pos.x = xInitR;
    pos.y = yInitR;
    obj.pos_initR = pos;
    pos.x = xEndL;
    pos.y = yEndL;
    obj.pos_endL = pos;
    obj.rayon = rayon;
    obj.forme = forme;
    obj.vx = vx;
    obj.vy = vy;
    obj.ay = ay;
    obj.vmodif = vmodif;
    return obj;
}

float checkCollision(objet ball, objet obj){
    int y_init = obj.pos_init.y;
    int x_init = obj.pos_init.x;
    int x;
    int y;
    float coef;
    int truc;
    int d;
    switch(obj.forme){
        int col = 0;
        case 0:
            coef = (float)(obj.pos_end.y-obj.pos_init.y)/(obj.pos_end.x-obj.pos_init.x);
            x = 0;
            y = obj.pos_end.y;
            truc = obj.pos_end.x-obj.pos_init.x;
            // printf("coef %f\n",coef);
            for(x=0;x<=fabs(truc);x++){
                y=(int)((coef*x)+(obj.pos_init.y));
                couleurCourante(0,255,0);
                epaisseurDeTrait(5);
                point(x+(obj.pos_init.x),y);
                /*if((pow((pos.x)-(x+obj.pos_init.x),2) + pow(pos.y - y,2)) <= pow(1,2))
                {            
                    printf("%d \t %d\n",pos.x,pos.y);
                    printf("%f \t %f \t %d \t %d\n", (pow((pos.x)-(x+obj.pos_init.x),2) + pow(pos.y - y,2)),pow(ball.rayon,2),x,y);
                }*/
                //printf("%d \t %d\n",x+obj.pos_init.x, pos.x);
                //printf("%d \t %d \t %d \t %d \t %f \t %d\n",y, pos.y, (x+obj.pos_init.x), pos.x,fabs(truc), truc );
                if((pow((ball.pos_init.x)-(x+obj.pos_init.x),2) + pow(ball.pos_init.y - y,2)) <= pow(ball.rayon,2))
                {
                    return coef;
                }
            }
            return 0;
            break;
        case 4:
        case 1:
            d = sqrt(pow((ball.pos_init.x-obj.pos_init.x),2) + pow((ball.pos_init.y-obj.pos_init.y),2));
            if(d <= (ball.rayon + obj.rayon)){
                return 1;
            }else{
                return 0;
            }
            break;
        case 2:
            y_init = obj.pos_init.y;
            x_init = obj.pos_init.x;
            while(y_init < obj.pos_end.y){
                if( (pow((obj.pos_init.x - ball.pos_init.x),2)+pow((y_init - ball.pos_init.y),2)) <= pow(ball.rayon,2)
                || (pow((obj.pos_end.x - ball.pos_init.x),2)+pow((y_init - ball.pos_init.y),2)) <= pow(ball.rayon,2)
                || (pow((obj.pos_endL.x - ball.pos_init.x),2)+pow((y_init - ball.pos_init.y),2)) <= pow(ball.rayon,2)
                || (pow((obj.pos_initR.x - ball.pos_init.x),2)+pow((y_init - ball.pos_init.y),2)) <= pow(ball.rayon,2) ){
                    return 1;
                }
                y_init++;
            }

            while(x_init < obj.pos_end.x){
                if( (pow((x_init - ball.pos_init.x),2)+pow((obj.pos_init.y - ball.pos_init.y),2)) <= pow(ball.rayon,2)
                || (pow((x_init - ball.pos_init.x),2)+pow((obj.pos_end.y - ball.pos_init.y),2)) <= pow(ball.rayon,2)
                || (pow((x_init - ball.pos_init.x),2)+pow((obj.pos_initR.y - ball.pos_init.y),2)) <= pow(ball.rayon,2)
                || (pow((x_init - ball.pos_init.x),2)+pow((obj.pos_endL.y - ball.pos_init.y),2)) <= pow(ball.rayon,2) ){
                    return 2;
                }
                x_init++;
            }
            return 0;
            break;
        case 3:
            y_init = obj.pos_init.y;
            x_init = obj.pos_init.x;
            while(y_init < obj.pos_end.y){
                if( (pow((obj.pos_init.x - ball.pos_init.x),2)+pow((y_init - ball.pos_init.y),2)) <= pow(ball.rayon,2)
                || (pow((obj.pos_end.x - ball.pos_init.x),2)+pow((y_init - ball.pos_init.y),2)) <= pow(ball.rayon,2)){
                    return 1;
                }
                y_init++;
            }

            while(x_init < obj.pos_end.x){
                if( (pow((x_init - ball.pos_init.x),2)+pow((obj.pos_init.y - ball.pos_init.y),2)) <= pow(ball.rayon,2)
                || (pow((x_init - ball.pos_init.x),2)+pow((obj.pos_end.y - ball.pos_init.y),2)) <= pow(ball.rayon,2)){
                    return 2;
                }
                x_init++;
            }
            return 0;
            break;
        case 5:
            coef = (float)(obj.pos_end.y-obj.pos_init.y)/(obj.pos_end.x-obj.pos_init.x);
            x = 0;
            y = obj.pos_end.y;
            truc = obj.pos_end.x-obj.pos_init.x;
            col = 0;
            for(x=0;x<=fabs(truc);x++){
                y=(int)((coef*x)+(obj.pos_init.y));
                couleurCourante(0,255,0);
                epaisseurDeTrait(5);
                if(ball.pos_init.y-ball.rayon <= y && ball.pos_init.x >= obj.pos_init.x && ball.pos_init.x <= obj.pos_end.x)
                {
                    col = 1;
                }
            }
            if(col == 1){
                coef = (float)((obj.pos_end.y-50)-obj.pos_init.y)/(obj.pos_end.x-obj.pos_init.x);
                x = 0;
                y = obj.pos_end.y;
                for(x=0;x<=fabs(truc);x++){
                    y=(int)((coef*x)+(obj.pos_init.y-50));
                    couleurCourante(0,255,0);
                    epaisseurDeTrait(5);
                    if(ball.pos_init.y-ball.rayon <= y && ball.pos_init.x >= obj.pos_init.x && ball.pos_init.x <= obj.pos_end.x)
                    {
                        return 1;
                    }
                }
            }
        case 6:
            coef = (float)(obj.pos_end.y-obj.pos_init.y)/(obj.pos_end.x-obj.pos_init.x);
            x = 0;
            y = obj.pos_end.y;
            truc = obj.pos_end.x-obj.pos_init.x;
            col = 0;
            for(x=0;x<=fabs(truc);x++){
                y=(int)((coef*x)+(obj.pos_init.y));
                couleurCourante(0,255,0);
                epaisseurDeTrait(5);
                if(ball.pos_init.y+ball.rayon >= y && ball.pos_init.y-ball.rayon <= y && ball.pos_init.x >= obj.pos_init.x && ball.pos_init.x <= obj.pos_end.x)
                {
                    col = 1;
                }
            }
            if(col == 1){
                coef = (float)(obj.pos_end.y-(obj.pos_init.y-50))/(obj.pos_end.x-obj.pos_init.x);
                x = 0;
                y = obj.pos_end.y;
                for(x=0;x<=fabs(truc);x++){
                    y=(int)((coef*x)+(obj.pos_init.y-50));
                    couleurCourante(0,255,0);
                    epaisseurDeTrait(5);
                    if(ball.pos_init.y+ball.rayon >= y && ball.pos_init.y-ball.rayon <= y && ball.pos_init.x >= obj.pos_init.x && ball.pos_init.x <= obj.pos_end.x)
                    {
                        return 1;
                    }
                }
            }
            return 0;         
    }
    return -1;
}


objet calculCollision(float col, objet ball, objet obj){
    switch(obj.forme){
        case 0:
            if(col != 0)
			{
				if(ball.vy <= 0){
					if(col < 0){
						if(ball.vx < 0){
                            ball.vx= -ball.vx;
                            ball.pos_init.y += 4;
						}else /*if(ball.vx == 0)*/{
                            if(ball.vy <= 0 && ball.vy > -1){
                                ball.vx = 5;
                            }
                            ball.vx = fabs(ball.vy/2);
                            ball.pos_init.y += 5;
                            if(ball.vy <= 0 && ball.vy > -1){
                                ball.pos_init.y -= 4;
                            }
                        }
						ball.vx = 1+(3*ball.vx/4);
						ball.pos_init.x += 4;
					}else{
						if(ball.vx > 0){
							ball.vx = -ball.vx;
                            ball.pos_init.y += 4;
						}else if(ball.vx == 0){
                            if(ball.vy <= 0 && ball.vy > -1){
                                ball.vx = -5;
                            }
                            ball.vx = -fabs(ball.vy/2);
                            ball.pos_init.y += 5;
                            if(ball.vy <= 0 && ball.vy > -1){
                                ball.pos_init.y -= 4;
                            }
                        }
						ball.vx = (3*ball.vx/4)-1;
						ball.pos_init.x -= 4;
                    }
				}else{
					if(col < 0){
						if(ball.vx >= 0){
							ball.vx = -ball.vx;
                            ball.pos_init.y -= 4;
						}else{
                            ball.pos_init.y += 4;
                        }
						if(ball.vx == 0){
							ball.vx = -5*fabs(ball.vy/4);
							ball.vy = 0;
						}else{
							ball.vx = -5*fabs(ball.vy/4);
						}
						ball.pos_init.x -= 4;
					}else{
						if(ball.vx <= 0){
							ball.vx = -ball.vx;
                            ball.pos_init.y -= 4;
						}else{
                            ball.pos_init.y += 4;
                        }
						if(ball.vx == 0){
							ball.vx = 5*fabs(ball.vy/4);
							ball.vy = 0;
						}else{
							ball.vx = 5*fabs(ball.vy/4);
						}
						ball.pos_init.x += 4;
					}
					ball.vx = ball.vx/2;					
				}
				ball.vy = -3*ball.vy/4;
			}
            break;
        case 1:
        case 4:
            if(col == 1){
				if(ball.pos_init.x >= obj.pos_init.x){
					ball.vx = 3*fabs(ball.vy/4);
				}
				if(ball.pos_init.x <= obj.pos_init.x){
					ball.vx = -3*fabs(ball.vy/4);
				}
				ball.vy = -ball.vy;
				ball.vx += 2;
				ball.vy += 2;
                if(ball.vx > 0){
                    ball.vx += obj.vmodif;
                }else{
                    ball.vx -= obj.vmodif;
                }
                if(ball.vy > 0){
                    ball.vy += obj.vmodif;
                }else{
                    ball.vy -= obj.vmodif;
                }
			}
            break;
        case 2:
        case 3:
            if(col != 0){
				if(col == 1){
					if(ball.vx > 0){
                        ball.pos_init.x -= 4;
                    }else if(ball.vx < 0){
                        ball.pos_init.x += 4;
                    }
                    ball.vx = -ball.vx;		
                    ball.vx = 3*ball.vx/4;
				}else{
					if(ball.vy < 0){
						ball.pos_init.y += 5;
					}else{
						ball.pos_init.y -= 5;
					}
					ball.vy = -ball.vy;
					ball.vy = (float)(0.90*(float)ball.vy);
				}
			}
            break;
        case 5:
            if(col != 0){
                if(ball.vx > 0){
                    ball.vx += obj.vmodif;
                }else{
                    ball.vx -= obj.vmodif;
                }
                if(ball.vy > 0){
                    ball.vy += obj.vmodif;
                }else{
                    ball.vy -= obj.vmodif;
                }
                ball.pos_init.y = 140 + ball.rayon;
                ball.vx = fabs(ball.vx)/2;
                ball.vy = fabs(ball.vy);
            }
            break;
        case 6:
            if(col != 0){
                if(ball.vx > 0){
                    ball.vx += obj.vmodif;
                }else{
                    ball.vx -= obj.vmodif;
                }
                if(ball.vy > 0){
                    ball.vy += obj.vmodif;
                }else{
                    ball.vy -= obj.vmodif;
                }
                ball.pos_init.y = 140 + ball.rayon;
                ball.vx = -fabs(ball.vx)/2;
                ball.vy = fabs(ball.vy);                
            }
            break;
    }
    return ball;
}