/*
File    : HMI.c
Author  : Yanis ATTIA
Created : 18/12/2017
Comment : File that contains the different functions for displaying the different items of the hmi
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

#include "gfxlib/GfxLib.h"
#include "gfxlib/BmpLib.h"
#include "gfxlib/ESLib.h"

#include "define.h"
#include "hmi.h"

    /*
    bouton{
        char *nom;
        int etat;
        position pos;
        DonneesImageRGB *bpSprite;
    }
    */

void dispBouton(bouton button){
    if(button.bpSprite != NULL)
        ecrisImage(button.pos.x, 
        button.pos.y, 
        button.bpSprite->largeurImage, 
        button.bpSprite->hauteurImage, 
        button.bpSprite->donneesRGB);
}

void gereClic(bouton buttons[NB_BUTTONS]){
    for(int i = 0; i < NB_BUTTONS; i++){
        if(abscisseSouris() > buttons[i].pos.x && 
        abscisseSouris() < (buttons[i].bpSprite->largeurImage)+buttons[i].pos.x &&
        ordonneeSouris() > buttons[i].pos.y && 
        ordonneeSouris() < (buttons[i].bpSprite->hauteurImage)+buttons[i].pos.y){
            buttons[i].etat = 1;
        }
    }
}

void gestionBouton(bouton buttons[NB_BUTTONS], int state[NB_STATE]){
    //Nouvelle partie
    if(buttons[0].etat == 1 && state[0] == 0){
        //On affiche la fenetre de jeu (on sort du menu principal)
        state[0] = 1;
        buttons[0].etat = 0;
        //On remet le menu pause à 0 au cas ou l'utilisateur aurait appuyé sur échap
        state[1] = 0;
        printf("%s()\n", buttons[0].nom);
    }

    //Affichage des règles
    else if(buttons[1].etat == 1 && state[0] == 0){
        buttons[1].etat = 0;
        state[1] = 2;
        printf("%s()\n", buttons[1].nom);
    }

    //Quitte le jeu
    else if(buttons[2].etat == 1 && state[0] == 0){
        buttons[2].etat = 0;
        system("pkill mpg123");
        printf("%s()\n", buttons[2].nom);
        //On met fin à l'application
        termineBoucleEvenements();
    }

    //Resume game
    else if(buttons[3].etat == 1 && state[1] == 1){
        buttons[3].etat = 0;
        state[1] = 0;
        printf("%s()\n", buttons[3].nom);
    }
    
    //Rules
    else if(buttons[4].etat == 1 && state[1] == 1){
        buttons[4].etat = 0;
        state[1] = 2;
        printf("%s()\n", buttons[4].nom);
    }

    //exit to menu
    else if(buttons[5].etat == 1 && state[1] == 1){
        buttons[5].etat = 0;
        state[1] = 0;
        state[2] = 0;
        state[0] = 0;
        printf("%s()\n", buttons[5].nom);
        buttons[2].etat = 0;
    }
    
    //or, set all buttons back to normal state
    else{
        for(int i = 0; i < NB_BUTTONS; i++){
            buttons[i].etat = 0;
        }
    }
}

void dispWindow(DonneesImageRGB *windowImg[NB_IMG], DonneesImageRGB *ressortImg[NB_IMG], objet ball, int state[NB_STATE], bouton buttons[NB_BUTTONS]){
    
    /*
    Affichage des fenetres de fond et des boutons si nécessaire
    */

    //On regarde si l'on est bien sur l'écran d'accueil
	if(state[0] == 0){
		//Alors, on affiche le menu principal
		//Affichage de l'image de fond (si l'image est bien initialisée)
		if(windowImg[0] != NULL)
			ecrisImage(0, 0, windowImg[0]->largeurImage, windowImg[0]->hauteurImage, windowImg[0]->donneesRGB);
		//Après quoi on affiche chaque bouton du menu principal
		dispBouton(buttons[0]);
		dispBouton(buttons[1]);
		dispBouton(buttons[2]);
	}

    //On regarde si l'on est bien sur l'écran du jeu
    if(state[0] == 1){
        //On affiche la zone de jeu
        dispGameZone(windowImg, ressortImg, state);
        couleurCourante(ball.color.R, ball.color.G, ball.color.B);
        cercle(ball.pos_init.x, ball.pos_init.y, ball.rayon);
    }

    /*
    Affichage des pop ups et des boutons si nécessaire
    */

    //Si le popup du menu pause est activé ET que le menu actif est celui du jeu
    if(state[1] == 1 && state[0] == 1){
        //Si l'image du menu pause est bien définie, on l'affiche au centre de l'écran
        if(windowImg[3] != NULL)
            ecrisImage(200, 100, windowImg[3]->largeurImage, windowImg[3]->hauteurImage, windowImg[3]->donneesRGB);
        dispBouton(buttons[3]);
		dispBouton(buttons[4]);
		dispBouton(buttons[5]);
    }   

    if(state[1] == 2){
        if(windowImg[5] != NULL)
            ecrisImage(50, 50, windowImg[5]->largeurImage, windowImg[5]->hauteurImage, windowImg[5]->donneesRGB); 
        dispBouton(buttons[6]);
    }
}

void dispGameZone(DonneesImageRGB *windowImg[NB_IMG], DonneesImageRGB *ressortImg[NB_IMG], int state[NB_STATE]){
    char score[30];
    char temp[20];

    //Initialisation des positions de la zone du flipper
    position playZoneMin;
    playZoneMin.x = 310;
    playZoneMin.y = 10;
    position playZoneMax;
    playZoneMax.x = 790;
    playZoneMax.y = 590;


    //Position des flippers
    //Flipper gauche
    position flipperLeftD;
    flipperLeftD.x = 444;
    flipperLeftD.y = 110;
    position flipperLeftF;
    flipperLeftF.x = 505;
    //La position du flipper change si le bouton 's' est appuyé
    if(state[3] == 1)
        flipperLeftF.y = 140;
    else   
        flipperLeftF.y = 90;
    
    //Flipper droit
    position flipperRightD;
    flipperRightD.x = 545;
    //La position du flipper change si le bouton 'l' est appuyé
    if(state[4] == 1)
        flipperRightD.y = 140;
    else   
        flipperRightD.y = 90;
    position flipperRightF;
    flipperRightF.x = 606;
    flipperRightF.y = 110;

    couleur chanAlpha;
    chanAlpha.R = 255;
    chanAlpha.G = 0;
    chanAlpha.B = 255;
    
    //Affichage du score sur la gauche
    epaisseurDeTrait(2);
    couleurCourante(255, 255, 255);
    rectangle(10, hauteurFenetre()-70, 300, hauteurFenetre()-10);
    couleurCourante(0, 0, 0);
    rectangle(20, hauteurFenetre()-60, 290, hauteurFenetre()-20);
    couleurCourante(255, 255, 255);
    sprintf(temp, "%d", state[8]);
    strcpy(score, "SCORE: ");
    strcat(score, temp);
    afficheChaine(score, 30, 25, hauteurFenetre()-55);

    //Affichage du High Score
    if(state[9] != 0){
        couleurCourante(255, 255, 255);
        rectangle(10, hauteurFenetre()-140, 300, hauteurFenetre()-80);
        couleurCourante(0, 0, 0);
        rectangle(20, hauteurFenetre()-130, 290, hauteurFenetre()-90);
        couleurCourante(255, 255, 255);
        sprintf(temp, "%d", state[9]);
        strcpy(score, "BEST : ");
        strcat(score, temp);
        afficheChaine(score, 30, 25, hauteurFenetre()-125);
    }

    //Affichage des noms
    couleurCourante(255, 255, 255);
    rectangle(10, 10, 300, 155);
    couleurCourante(0, 0, 0);
    rectangle(20, 20, 290, 145);
    couleurCourante(255, 255, 255);
    afficheChaine("NOWEL PINBALL", 24, 25, 115);
    afficheChaine("Yanis", 24, 25, 55);afficheChaine("ATTIA", 24, 120, 55);
    afficheChaine("Arthur FRANCISCO", 24, 25, 25);

    //On affiche le fond du piball (si l'image est bien initialisée)
    if(state[2] == 0){
        if(windowImg[1] != NULL)
            ecrisImage(playZoneMin.x, playZoneMin.y, windowImg[1]->largeurImage, windowImg[1]->hauteurImage, windowImg[1]->donneesRGB);
    }else if(state[2] == 1){
        if(windowImg[2] != NULL)
        ecrisImage(playZoneMin.x, playZoneMin.y, windowImg[2]->largeurImage, windowImg[2]->hauteurImage, windowImg[2]->donneesRGB);        
    }
    if(windowImg[4] != NULL)
        ecrisImageSansFond(playZoneMin.x, playZoneMin.y, windowImg[4]->largeurImage, windowImg[4]->hauteurImage, windowImg[4]->donneesRGB, chanAlpha);
    
    //Affichage du ressorts
    if(ressortImg[state[5]] != NULL)
        ecrisImageSansFond(playZoneMax.x-50, playZoneMin.y+5, ressortImg[state[5]]->largeurImage, ressortImg[state[5]]->hauteurImage, ressortImg[state[5]]->donneesRGB, chanAlpha);

    //On affiche les flippers
    couleurCourante(255, 0, 0);
    epaisseurDeTrait(5);
    ligne(flipperLeftD.x, flipperLeftD.y, flipperLeftF.x, flipperLeftF.y);
    ligne(flipperRightD.x, flipperRightD.y, flipperRightF.x, flipperRightF.y);
}