#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

#include "gfxlib/GfxLib.h"
#include "gfxlib/BmpLib.h"
#include "gfxlib/ESLib.h"

#include "define.h"
#include "hmi.h"
#include "calculus.h"

void gestionEvenement(EvenementGfx evenement);

int main(int argc, char **argv)
{
	initialiseGfx(argc, argv);
	prepareFenetreGraphique("PinBall Nowel", LargeurFenetre, HauteurFenetre);
	lanceBoucleEvenements();
	
	return 0;
}

void gestionEvenement(EvenementGfx evenement)
{
	/*
	- tableau d'états
	+ 0 + variable d'état pour la fenêtre                (0/1)
	+ 1 + variable d'état pour les popUps                (0/1/2)
	+ 2 + variable d'état pour le lancement de la partie (0/1)
	+ 3 + variable d'état pour le flipper de gauche      (0/1)
	+ 4 + variable d'état pour le flipper de droite      (0/1)
	+ 5 + variable d'état pour le ressort                (0/1/2/3/4/5)
	+ 6 + temporisation	(flipper gauche)
	+ 7 + temporisation (flipper droit)
	+ 8 + score
	+ 9 + high score
	*/
	static int state[NB_STATE];
	
	/*
	- tableau de boutons
	+ 0 + bouton nouvelle partie (main menu)
	+ 1 + bouton affichage des règles (main menu)
	+ 2 + bouton quitter (main menu)
	*/
	static bouton buttons[NB_BUTTONS];

	/*
	- tableau d'images pour le fond de la fenetre
	+ 0 + menu principal (images/menu_bg.bmp)
	+ 1 + fond du pinball off
	+ 2 + fond du pinball on
	+ 3 + menu pause
	+ 4 + game obstacle
	+ 5 + rules
	*/
	static DonneesImageRGB *windowImg[NB_IMG];
	
	/*
	- tableau d'images pour le ressort
	+ 0 + initial state
	+ 1 + 25%
	+ 2 + 50%
	+ 3 + 75%
	+ 4 + 100%
	*/
	static DonneesImageRGB *ressortImg[NB_RESSORT];
	
	//Tableau d'objets
	static objet obj[NB_OBJETS];

	//Objet pour la balle
	static objet ball;

	static int etatFlipper;

	switch (evenement)
	{
		case Initialisation:
			//Initialisation des images de fond
			windowImg[0] = lisBMPRGB("images/menu_bg.bmp");
			windowImg[1] = lisBMPRGB("images/playground/pinball_off.bmp");
			windowImg[2] = lisBMPRGB("images/playground/pinball_on.bmp");
			windowImg[3] = lisBMPRGB("images/menu_pause.bmp");
			windowImg[4] = lisBMPRGB("images/game_obst.bmp");
			windowImg[5] = lisBMPRGB("images/rules.bmp");
			
			//Initialisation des images du ressort
			ressortImg[0] = lisBMPRGB("images/playground/ressort0.bmp");
			ressortImg[1] = lisBMPRGB("images/playground/ressort1.bmp");
			ressortImg[2] = lisBMPRGB("images/playground/ressort2.bmp");
			ressortImg[3] = lisBMPRGB("images/playground/ressort3.bmp");
			ressortImg[4] = lisBMPRGB("images/playground/ressort4.bmp");
			
			//Initialisation des objets
			obj[0] = initialiseObjet("", 370, 440, 490, 540, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0);
			
			obj[1] = initialiseObjet("", 720, hauteurFenetre()-10, largeurFenetre()-10, 520, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			
			obj[2] = initialiseObjet("", 370, 208, 370, 312, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0);			
			obj[3] = initialiseObjet("", 369, 209, 452, 158, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			obj[4] = initialiseObjet("", 370, 310, 450, 160, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			
			obj[5] = initialiseObjet("bumper", 460, 350, 0, 0, 0, 0, 0, 0, 30, 1, 3, 0, 0, 0);
			obj[6] = initialiseObjet("bumper", 595, 350, 0, 0, 0, 0, 0, 0, 30, 1, 3, 0, 0, 0);

			obj[7] = initialiseObjet("", 602, 158, 685, 310, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);			
			obj[8] = initialiseObjet("", 602, 157, 685, 188, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			obj[9] = initialiseObjet("", 685, 188, 685, 310, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0);

			/*
			obj[10] = initialiseObjet("", 490, 230, 530, 310, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);			
			obj[11] = initialiseObjet("", 530, 310, 570, 230, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			obj[12] = initialiseObjet("", 488, 230, 572, 230, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0);
			*/

			obj[13] = initialiseObjet("", 735, 10, 735, 500, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0);

			obj[14] = initialiseObjet("", 345, 180, 452, 108, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

			obj[15] = initialiseObjet("", 605, 110, 735, 160, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			obj[16] = initialiseObjet("", 470, 10, 470, 50, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0);

			obj[17] = initialiseObjet("", 310, 150, 470, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

			obj[18] = initialiseObjet("", 655, 460, 0, 0, 0, 0, 0, 0, 40, 1, 0, 0, 0, 0);
			
			obj[19] = initialiseObjet("", largeurFenetre()-60, 75, largeurFenetre(), 75, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0);			
			
			obj[22] = initialiseObjet("", 448, 111, 0, 0, 0, 0, 0, 0, 2, 1, 0, 0, 0, 0);

			obj[20] = initialiseObjet("", 442, 112, 505, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			obj[21] = initialiseObjet("", 545, 90, 608, 110, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			ball = initialiseObjet("balle", largeurFenetre()- 15 - ressortImg[0]->largeurImage/2, ressortImg[0]->hauteurImage+25, 0, 0, 0, 0, 0, 0, BALL_RAY, 4, 0, 0, 0, 0);
			ball.color.R = 125;
			ball.color.G = 0;
			ball.color.B = 125;
			
			initState(state);
			initBoutons(buttons);
			system("mpg123 -q --loop -1 theme.mp3 &"); 
			demandeTemporisation(10);
			break;
		
		case Temporisation:
			if(state[1] == 0 && state[0] == 1){
				etatFlipper++;
				if(state[6] == 1 || state[7] == 1)
					etatFlipper = 0;
				if(etatFlipper == 10){
					state[3] = 0;
					state[4] = 0;
					obj[20].pos_end.y = 90;
					obj[21].pos_init.y = 90;
					obj[20].vmodif = 0;
					obj[21].vmodif = 0;
					obj[21].forme = 0;
					obj[20].forme = 0;
					etatFlipper = 0;
				}
				obj[19].pos_init.y = 14+ressortImg[state[5]]->hauteurImage;
				obj[19].pos_end.y = 14+ressortImg[state[5]]->hauteurImage;
				//Rebond de balle
				if (ball.pos_init.x <= 310 + ball.rayon || ball.pos_init.x >= largeurFenetre() - 10 - ball.rayon){
					system("mpg123 -q bounce.mp3 &");
					if(ball.vx > 0){
						ball.pos_init.x -= 4;
					}else if(ball.vx < 0){
						ball.pos_init.x += 4;
					}
					ball.vx = -ball.vx;		
					ball.vx = 3*ball.vx/4;		
				}
				if (ball.pos_init.y >= (hauteurFenetre() - (10 + ball.rayon))){
					system("mpg123 -q bounce.mp3 &");
					if(ball.vy > 0){
						ball.pos_init.y -= 4;
					}else if(ball.vy < 0){
						ball.pos_init.y += 4;
					}
					if(ball.vy != 0){
						ball.vy = -ball.vy;
					}
					ball.vy = (float)(0.90*(float)ball.vy);
				}
	
				//FIN DE LA MANCHE
				if((ball.pos_init.y <= ball.rayon)){
					ball = initialiseObjet("balle", largeurFenetre()- 15 - ressortImg[0]->largeurImage/2, ressortImg[0]->hauteurImage+25, 0, 0, 0, 0, 0, 0, BALL_RAY, 4, 0, 0, 0, 0);
					system("mpg123 -q lost.mp3 &");
					ball.color.R = 125;
					ball.color.G = 0;
					ball.color.B = 125;			
					state[2] = 0;
					state[5] = 0;
					if(state[8] > state[9])
						state[9] = state[8];
					state[8] = 0;
				}

				float col;
				for(int i = 0;i<NB_OBJETS;i++){
					col = checkCollision(ball, obj[i]);
					ball = calculCollision(col, ball, obj[i]);
					if(state[2] == 1 && col != 0){
						system("mpg123 -q bounce.mp3 &");
						ball.color.R += (valeurIntervalleZeroUn()*2550);
						ball.color.B += (valeurIntervalleZeroUn()*2550);
						ball.color.R %= 255;
						ball.color.B %= 255;

						if(i == 5 || i == 6){
							system("mpg123 -q bumper.mp3 &");
							state[8] += 20;
						}
						else if(i == 17){
							system("mpg123 -q bounce.mp3 &");							
							state[8] = state[8];
						}
						else if(i == 18){
							system("mpg123 -q bumper.mp3 &");
							state[8] += 20;
						}
						else if(i == 20 || i == 21){
							system("mpg123 -q bounce.mp3 &");							
							state[8] += 15;
						}
						else if(i == 19 && (ball.vy < 5 || ball.vy > -5)){
							state[2] = 0;
							ball = initialiseObjet("balle", largeurFenetre()- 15 - ressortImg[0]->largeurImage/2, ressortImg[0]->hauteurImage+25, 0, 0, 0, 0, 0, 0, BALL_RAY, 4, 0, 0, 0, 0);
							ball.color.R = 125;
							ball.color.G = 0;
							ball.color.B = 125;
						}
						else{
							system("mpg123 -q bounce.mp3 &");
							state[8] += 5;	
						}
					}
				}
				if(ball.vx >= 25){
					ball.vx = 24;
				}
				if(ball.vy >= 25){
					ball.vy = 24;
				}
				ball.pos_init.x += ball.vx;
				ball.vy += ball.ay;
				ball.pos_init.y += ball.vy;
				if(ball.pos_init.y <= ball.rayon)
					ball.pos_init.y = ball.rayon;
				state[6] = 0;
				state[7] = 0;
			}
			gestionBouton(buttons, state);
			rafraichisFenetre();
			break;
			
		case Affichage:
			effaceFenetre(0, 0, 0);
			dispWindow(windowImg, ressortImg, ball, state, buttons);	
			/*
			for(int i=0;i<NB_OBJETS;i++){
				switch(obj[i].forme){
					case 0:
					case 3:
						ligne(obj[i].pos_init.x,obj[i].pos_init.y,obj[i].pos_end.x,obj[i].pos_end.y);
						break;
					case 1:
						cercle(obj[i].pos_init.x,obj[i].pos_init.y,obj[i].rayon);
						break;
					case 2:
						rectangle(obj[i].pos_init.x,obj[i].pos_init.y,obj[i].pos_end.x,obj[i].pos_end.y);
						break;
				}
			}
			*/
			break;
			
		case Clavier:
			printf("%c : ASCII %d\n", caractereClavier(), caractereClavier());
			switch (caractereClavier())
			{
				case 'Q':
				case 'q':
					system("pkill mpg123");
					termineBoucleEvenements();
					break;

				case 27:
					if(state[1] == 0)
						state[1] = 1;
					else
						state[1] = 0;
				break;

				case 'r':
				case 'R':
					ball = initialiseObjet("balle", largeurFenetre()- 15 - ressortImg[0]->largeurImage/2, ressortImg[0]->hauteurImage+25, 0, 0, 0, 0, 0, 0, BALL_RAY, 4, 0, 0, 0, 0);
					state[5] = 0;
					state[2] = 0;
					ball.color.R = 125;
					ball.color.G = 0;
					ball.color.B = 125;
				break;

				case '+':
					if(state[5] < 4 && state[1] == 0 && state[2] == 0){
						state[5]++;
						ball.pos_init.y -= 10;
					}
				break;

				case '-':
					if(state[5] > 0 && state[1] == 0  && state[2] == 0){
						state[5]--;
						ball.pos_init.y += 10;
					}
				break;

				case 32:
					if(state[2] != 1 && state[5] != 0){
						ball.vy = 2+(5*state[5]);
						ball.pos_init.y += 50;
						state[2] = 1;
						state[5] = 0;
						ball.ay = (-GRAVITY*sin(INCLINE/180*M_PI))/10;
						system("mpg123 -q launch.mp3 &");
					}
				break;

				case 's':
				case 'S':
					state[3] = 1;
					state[6] = 1;
					obj[20].pos_end.y = 140;
					obj[20].vmodif = 10;
					obj[20].forme = 6;
					system("mpg123 -q left-flip.mp3 &");
				break;

				case 'l':
				case 'L':
					state[4] = 1;
					state[7] = 1;
					obj[21].pos_init.y = 140;
					obj[21].vmodif = 10;
					obj[21].forme = 6;
					system("mpg123 -q right-flip.mp3 &");
				break;
				
				case 'B':
				case 'b':
					state[3] = 1;
					state[4] = 1;
					state[6] = 1;
					obj[20].pos_end.y = 140;
					obj[21].pos_init.y = 140;
				break;
			}
			break;
			
		case ClavierSpecial:
			break;

		case BoutonSouris:
			if (etatBoutonSouris() == GaucheAppuye){
			}
			else if (etatBoutonSouris() == GaucheRelache){
				gereClic(buttons);
			}
			break;
		
		case Souris:
			break;
		
		case Inactivite:
			break;
		
		case Redimensionnement:
			printf("Largeur : %d\t", largeurFenetre());
			printf("Hauteur : %d\n", hauteurFenetre());		
			break;
	}
}
