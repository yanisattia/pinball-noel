/*
File    : calculus.h
Author : Arthur FRANCISCO
Created : 18/12/2017
Comment : File that contains all the definitions used in the project.
*/

/*
----------------------- DEFINITION OF CALCULING FUNCTIONS -----------------------
*/

objet initialiseObjet(char nom[LCH], int xInit, int yInit, int xEnd, int yEnd, int xInitR, int yInitR, int xEndL, int yEndL, int rayon, int forme, int vmodif, float vx, float vy, float ay);
float checkCollision(objet ball, objet obj);
objet calculCollision(float col, objet ball, objet obj);