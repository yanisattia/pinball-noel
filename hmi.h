/*
File    : HMI.h
Author  : Yanis ATTIA
Created : 18/12/2017
Comment : File that contains the different functions for displaying the different items of the hmi
*/

/*
--------------- DEFINITION OF INITIALISATION FUNCTIONS --------------
*/

void dispBouton(bouton button);
void gereClic(bouton buttons[NB_BUTTONS]);
void gestionBouton(bouton buttons[NB_BUTTONS], int state[NB_STATE]);

void dispWindow(DonneesImageRGB *windowImg[NB_IMG], DonneesImageRGB *ressortImg[NB_IMG], objet ball, int state[NB_STATE], bouton buttons[NB_BUTTONS]);
void dispGameZone(DonneesImageRGB *windowImg[NB_IMG], DonneesImageRGB *ressortImg[NB_IMG], int state[NB_STATE]);